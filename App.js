/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  Button,
  ActivityIndicator,
} from 'react-native';
import axios from 'axios';

const App = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [isLogin, setLogin] = useState(false);
  const [isLoading, setLoading] = useState(false);
  const [isValid, setValid] = useState(true);

  const handleValid = () => {
    setValid(true);
  };

  const toggleLogin = () => {
    setLoading(true);
    const config = {
      username,
      password,
    };
    axios
      .post('https://tasklogin.herokuapp.com/api/login', config)
      .then(res => {
        if (res) {
          setLoading(false);
          setLogin(true);
        }
      })
      // eslint-disable-next-line handle-callback-err
      .catch(err => {
        setLoading(false);
        setValid(false);
      });
  };

  return (
    <View style={styles.container}>
      {!isLogin ? (
        <>
          <FormInput
            label="Username"
            value={username}
            style={styles.input(isValid)}
            handleChange={text => setUsername(text)}
            handleValid={handleValid}
          />
          <FormInput
            label="Password"
            value={password}
            style={styles.input(isValid)}
            handleChange={text => setPassword(text)}
            handleValid={handleValid}
            secureEntry
          />
          <Button
            onPress={() => toggleLogin()}
            title="LOGIN"
            disabled={isLoading}
          />
          {!isValid && (
            <Text style={styles.warning}>
              {' '}
              * Username atau Password anda salah
            </Text>
          )}
          {isLoading && (
            <ActivityIndicator
              style={styles.loading}
              size="large"
              color="black"
            />
          )}
        </>
      ) : (
        <HomeScreen />
      )}
    </View>
  );
};

const FormInput = ({
  label,
  style,
  value,
  handleChange,
  handleValid,
  secureEntry,
}) => {
  return (
    <>
      <Text>{label}</Text>
      <TextInput
        onFocus={handleValid}
        placeholder={`Silahkan masukan ${label} ..`}
        style={style}
        value={value}
        onChangeText={handleChange}
        secureTextEntry={secureEntry}
      />
    </>
  );
};

const HomeScreen = () => {
  return (
    <View>
      <Text>Hey halo salam kenal saya Alwi</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 20,
    height: '100%',
    justifyContent: 'center',
  },
  input: isValid => ({
    borderColor: !isValid ? 'red' : 'black',
    borderWidth: 1,
    borderRadius: 10,
    marginVertical: 10,
    padding: 10,
  }),
  warning: {color: 'red', marginVertical: 10},
  loading: {marginVertical: 10},
});

export default App;
